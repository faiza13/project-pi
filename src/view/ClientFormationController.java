/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import model.Formation;
import service.FormationDB;

/**
 * FXML Controller class
 *
 * @author foufou
 */
public class ClientFormationController implements Initializable {

     static  Stage stage;
    @FXML
     
    private ListView<ClientPanelController> listView;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
     ObservableList<ClientPanelController> paneList =FXCollections.observableArrayList();  
     ObservableList<Formation> formList =FXCollections.observableArrayList();  
        
         try {
             formList=FormationDB.getFormations();
         } catch (SQLException ex) {
             Logger.getLogger(MesFormationsController.class.getName()).log(Level.SEVERE, null, ex);
         }
       
        formList.stream().forEach(e -> {
    ClientPanelController pane = new    ClientPanelController();  
         try {
             pane.participeAction();
             pane.viewAction();
         } catch (SQLException ex) {
             Logger.getLogger(ClientFormationController.class.getName()).log(Level.SEVERE, null, ex);
         }
    pane.setIdVal(Integer.toString(e.getFormation_id()));
    pane.setNomVal(e.getNom());
    pane.setDateVal(e.getDate_debut());
    pane.setLieuVal(e.getAddress_formation());
    paneList.add(pane);
   
    
            
         
     });
  
    listView.setItems(paneList);
    
     
     
     } 

}
