/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Formation;
import model.Profil;
import service.FormationDB;
import service.ProfilDB;

/**
 * FXML Controller class
 *
 * @author foufou
 */
public class MesFormationsController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    
    static  Stage stage;
    @FXML
     
    private ListView<PanelFormationController> listView;
    public static int idSelected;
    public static String choix=null;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
     ObservableList<PanelFormationController> paneList =FXCollections.observableArrayList();  
     ObservableList<Formation> formList =FXCollections.observableArrayList();  
        
         try {
             formList=FormationDB.getFormations();
         } catch (SQLException ex) {
             Logger.getLogger(MesFormationsController.class.getName()).log(Level.SEVERE, null, ex);
         }
       
        formList.stream().forEach(e -> {
    PanelFormationController pane = new PanelFormationController();  
    pane.setIdVal(Integer.toString(e.getFormation_id()));
    pane.setNomVal(e.getNom());
    pane.setDateVal(e.getDate_debut());
    pane.setLieuVal(e.getAddress_formation());
    paneList.add(pane);
   
    
            
         
     });
  
    listView.setItems(paneList);
    
    listView.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> showFormationDetails(newValue));  
     
     } 

    public void refresh() {
        ObservableList<PanelFormationController> paneList =FXCollections.observableArrayList();  
     ObservableList<Formation> formList =FXCollections.observableArrayList();  
        
         try {
             Profil profil=ProfilDB.getProfilById(2);
             formList=FormationDB.getFormationListByCreator(profil.getId());
         } catch (SQLException ex) {
             Logger.getLogger(MesFormationsController.class.getName()).log(Level.SEVERE, null, ex);
         }
       
        formList.stream().forEach(e -> {
    PanelFormationController pane = new PanelFormationController();  
            try {
                pane.viewAction();
                //pane.deleteAction();
            } catch (SQLException ex) {
                Logger.getLogger(MesFormationsController.class.getName()).log(Level.SEVERE, null, ex);
            }
    pane.setIdVal(Integer.toString(e.getFormation_id()));
    pane.setNomVal(e.getNom());
    pane.setDateVal(e.getDate_debut());
    pane.setLieuVal(e.getAddress_formation());
    paneList.add(pane);    
     });
  
    listView.setItems(paneList);
    
    listView.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> showFormationDetails(newValue));  
     
    }
    
    
 private void showFormationDetails(PanelFormationController formation) {
    if (formation != null) {
   this.idSelected= Integer.parseInt(formation.getIdVal());
    
   
    } else {
        
    }
}

      
 /*   public static void showMainView() throws IOException{
    FXMLLoader loader=new FXMLLoader();
    loader.setLocation(MesFormationsController.class.getResource("MesFormations.fxml"));
    
    Scene scene =new Scene(loader.load(),800,600);
    stage =new Stage();
    stage.setScene(scene);
    stage.show();
    wini el panel eli feha el info emta3 el formation ??
    hani mchit ana ...
    ey 7otha fil projet w abda e5dim deja mizel ma3adech barcha
    allo khalini emchi brabu
    ti mzelo 2 interfaces haya fisa3 nkamlohom tawa 
    }*/
 
 

     
    
     @FXML
    public void editAction() throws IOException{
        int selectedIndex = listView.getSelectionModel().getSelectedIndex();
    if (selectedIndex >= 0){
     choix="edit";
      
    Parent root;
 
    
 
   stage = new Stage();
   root = FXMLLoader.load(getClass().getResource("EditAjoutPopup.fxml"));

   stage.setScene(new Scene(root));
   stage.setTitle("Edit formation");
   stage.initModality(Modality.APPLICATION_MODAL);
 
   stage.showAndWait(); }
    else {
        // Nothing selected.
        Alert alert = new Alert(AlertType.WARNING);
        
        alert.setTitle("pas de Selection");
        alert.setHeaderText("aucune formation Selectionnée");
        alert.setContentText("veuillez selectionner une formation");

        alert.showAndWait();
    }

       
   }

    
    @FXML
    public void ajoutAction() throws IOException{
        choix="ajout";
     
    
        Parent root;
 
    
 
   stage = new Stage();
   root = FXMLLoader.load(getClass().getResource("EditAjoutPopup.fxml"));

   stage.setScene(new Scene(root));
   stage.setTitle("Edit formation");
   stage.initModality(Modality.APPLICATION_MODAL);
 
   stage.showAndWait(); 

       
   }



}

