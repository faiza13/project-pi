/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Formation;
import model.Profil;
import service.FormationDB;
import service.ParticipantDB;
import service.ProfilDB;
import static view.MesFormationsController.stage;

/**
 * FXML Controller class
 *
 * @author foufou
 */
public class ClientPanelController extends HBox  {

    @FXML private Label idVal;
    @FXML private Label nomVal;
    @FXML private Label dateVal;
    @FXML private Label lieuVal;
    @FXML private Button participe;
    @FXML private Button view;
        
    public  static Profil  profil ;
      public static int idPanel;


 
    /*public void initButton() {
    
     participe.setOnAction((event) -> {
    Profil profil = null;
         try {
             profil = ProfilDB.getProfilById(2);
         } catch (SQLException ex) {
             Logger.getLogger(ClientPanelController.class.getName()).log(Level.SEVERE, null, ex);
         }
        
        ParticipantDB.addParticipant(Integer.parseInt(idVal.getText()),profil.getId());
});
    
    }*/
    public ClientPanelController() {
        
        
        
        //this.initButton();
        
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
        "ClientPanel.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public String getIdVal() {
        return idVal.getText();
    }
    public String getNomVal() {
        return nomVal.getText();
    }

    public String getDateVal() {
        return dateVal.getText();
    }

    public String getLieuVal() {
        return lieuVal.getText();
    }

    public void setNomVal(String value) {
        nomVal.setText(value);
    }

    public void setDateVal(String value) {
        dateVal.setText(value);
    }
    public void setLieuVal(String value) {
        lieuVal.setText(value);
    }
    public void setIdVal(String value) {
        idVal.setText(value);
    }
  
    protected void participeAction() throws SQLException{
        
      participe.setOnAction((event) -> {
            try {
            profil = ProfilDB.getProfilById(2);
            int participe=ParticipantDB.addParticipant(Integer.parseInt(idVal.getText()),profil.getId());
        if(participe == 0){
System.out.println("deja participé");
 Alert alert = new Alert(AlertType.WARNING);
        
        alert.setTitle("Participation deja effectué");
        alert.setContentText("vous etes deja participants ");

        alert.showAndWait();
} else {
           Alert dialog = new Alert(AlertType.INFORMATION);
dialog.setTitle("Participation");
dialog.setHeaderText("Participation avec success");
dialog.setContentText("");
dialog.showAndWait();
System.out.println("votre participation est bien effectuée");
}
        
                      
            } catch (SQLException ex) {
            
                
            }
        
       
});}
        
        
        
        
        
        
      
                
                    
                    
                        
                     
                        
        
       

        protected void viewAction() throws SQLException{

        view.setOnAction((event) -> {
    
 
    Parent root;
 
   stage = new Stage();
            try {
                  idPanel=Integer.parseInt(idVal.getText());

       root = FXMLLoader.load(getClass().getResource("ViewFormationClient.fxml"));
                
                
                stage.setScene(new Scene(root));
   stage.setTitle("Affiche formation");
   stage.initModality(Modality.APPLICATION_MODAL);
 
   stage.showAndWait(); 
            
       
            
            } catch (IOException ex) {
                Logger.getLogger(ClientPanelController.class.getName()).log(Level.SEVERE, null, ex);
            }

   
});
        
        
    

    
    
   
        }
}



