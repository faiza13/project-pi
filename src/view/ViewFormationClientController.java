/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import model.Formation;
import model.Profil;
import service.FormationDB;
import service.ParticipantDB;
import static view.ClientPanelController.idPanel;

/**
 * FXML Controller class
 *
 * @author foufou
 */
public class ViewFormationClientController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML Label nomVal;
    @FXML Label dateVal;
    @FXML Label lieuVal;
    @FXML Label descriptionVal;
    @FXML ListView listView;
    
    public void initialize(URL url, ResourceBundle rb) {
        
      
     Formation form ;  
        
         try {
             form=FormationDB.getFormationById(ClientPanelController.idPanel);
             nomVal.setText(form.getNom());
             dateVal.setText(form.getDate_debut());
             lieuVal.setText(form.getAddress_formation());
             descriptionVal.setText(form.getDescription());
             
         } catch (SQLException ex) {
             Logger.getLogger(MesFormationsController.class.getName()).log(Level.SEVERE, null, ex);
         }
     ObservableList<PanelParticipantController> paneList =FXCollections.observableArrayList();  
     ObservableList<Profil> participantList =FXCollections.observableArrayList(); 
     
        try {
            participantList=ParticipantDB.getparticipantsbyformation(ClientPanelController.idPanel);
        } catch (SQLException ex) {
            Logger.getLogger(ViewFormationClientController.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        participantList.stream().forEach(e -> {
    PanelParticipantController pane = new    PanelParticipantController();  
         
    pane.setNomVal(e.getNom());
    pane.setDescriptionVal(e.getDescription());
    pane.setEmailVal(e.getEmail());
    pane.setTelVal(e.getTel());
    paneList.add(pane);
   
    
            
         
     });
  
    listView.setItems(paneList);
    
   
    
            
         
     
  
    
    
     
     
     } 
    }    
    

