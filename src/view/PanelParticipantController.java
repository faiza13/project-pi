/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author foufou
 */
public class PanelParticipantController extends HBox{

    /**
     * Initializes the controller class.
     */
    @FXML Label nomVal;
    @FXML Label prenomVal;  
    @FXML Label telVal;
    @FXML Label emailVal;
    @FXML Label descriptionVal;
            
   public PanelParticipantController() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
        "PanelParticipant.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public String getPrenomVal() {
        return prenomVal.getText();
    }
    public String getNomVal() {
        return nomVal.getText();
    }

    public String getDescriptionVal() {
        return descriptionVal.getText();
    }

    public String getEmailVal() {
        return emailVal.getText();
    }

     public String getTelVal() {
        return telVal.getText();
    }
    
     public void setNomVal(String value) {
        nomVal.setText(value);
    }

    public void setPrenomVal(String value) {
        prenomVal.setText(value);
    }
    public void setTelVal(String value) {
        telVal.setText(value);
    }
    public void setEmailVal(String value) {
        emailVal.setText(value);
    }
    public void setDescriptionVal(String value) {
        descriptionVal.setText(value);
    }


    
   
}
