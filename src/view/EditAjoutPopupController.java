/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import Commun.Commun;
import Technique.MyConnection;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import model.Formation;
import model.Profil;
import service.FormationDB;
import service.ProfilDB;

/**
 * FXML Controller class
 *
 * @author foufou
 */
public class EditAjoutPopupController implements Initializable {

    @FXML
    private TextField id;
    @FXML
    private TextField nom;
    @FXML
    private DatePicker dateDebut;
    @FXML
    private TextField adresse;
    @FXML
    private TextArea description;
    @FXML
    private Button btn;

    /**
     * Initializes the controller class.
     */
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        id.setVisible(false);
        if(MesFormationsController.choix=="edit"){
        try {
           

            FillFields(MesFormationsController.idSelected);
        } catch (ParseException ex) {
            Logger.getLogger(EditAjoutPopupController.class.getName()).log(Level.SEVERE, null, ex);
        }
        btn.setText("Edit");
           }
        else if (MesFormationsController.choix=="ajout"){
            id.setText(null);
           btn.setText("Add") ;  }          
        /*btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                buttonAction(event);
            }
        });*/
    }
    
    public void FillFields(int id_formation) throws ParseException{
        Formation formation;
        try {
            formation = FormationDB.getFormationById(id_formation);
            id.setText(Integer.toString(formation.getFormation_id()));
            nom.setText(formation.getNom());
            dateDebut.setValue(LocalDate.parse(formation.getDate_debut()));
            
            description.setText(formation.getDescription());
            adresse.setText(formation.getAddress_formation());
        } catch (SQLException ex) {
            Logger.getLogger(EditAjoutPopupController.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }
  
/*@FXML
    public void buttonAction(ActionEvent event){
       // description.setText("blaa");
       System.out.println("id :"+id.getText()+" length :");
        try { 
                Profil profil = ProfilDB.getProfilById(2);
                String date = Commun.Commun.datePickeToString(dateDebut);
                
                if(id.getText().length() == 0 || id.getText().equals("")){
                Formation formation = new Formation(0,this.nom.getText(),this.description.getText(),date,Commun.Commun.currentDate(),Commun.Commun.currentDate(),profil,this.adresse.getText(),"C://");
                FormationDB.addFormation(formation);
                    }
                else{
                Formation formation = new Formation(Integer.valueOf(id.getText()),this.nom.getText(),this.description.getText(),date,Commun.Commun.currentDate(),Commun.Commun.currentDate(),profil,this.adresse.getText(),"C://");
                FormationDB.updateFormation(formation);
     }
                } catch (SQLException ex) {
                Logger.getLogger(EditAjoutPopupController.class.getName()).log(Level.SEVERE, null, ex);
            }

       
   }

    */
    @FXML
    public void buttonAction() throws SQLException, IOException{
       
       
                Profil profil = ProfilDB.getProfilById(2);
                String date = Commun.datePickeToString(dateDebut);
                
                if(id.getText()==null){
                Formation formation = new Formation(0,this.nom.getText(),this.description.getText(),Commun.datePickeToString(dateDebut),Commun.currentDate(),Commun.currentDate(),profil,this.adresse.getText(),"C://");
                FormationDB.addFormation(formation);
                MesFormationsController.choix=null;
                    }
                else{
                Formation formation = new Formation(Integer.parseInt(id.getText()),this.nom.getText(),this.description.getText(),Commun.datePickeToString(dateDebut),Commun.currentDate(),Commun.currentDate(),profil,this.adresse.getText(),"C://");
                FormationDB.updateFormation(formation);
                MesFormationsController.choix=null;
     }
FXMLLoader loader=new FXMLLoader();
    loader.setLocation(MesFormationsController.class.getResource("MesFormations_1.fxml"));
    
    Scene scene =new Scene(loader.load(),800,600);
    testMesFormations.stage.setScene(scene);
                MesFormationsController.stage.close();
       
   }

}
