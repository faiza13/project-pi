/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import Technique.MyConnection;
import java.sql.PreparedStatement;
import model.Formation;
import model.FormationList;
import model.Profil;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author foufou
 */
public class FormationDB {
           public static void addFormation(Formation formation){
        String sql = "insert into formation(nom,description,creation_date,last_modification_date"
                + ",date_debut,creator_id,address_formation,photo_profil_path) values("
                + "'"+formation.getNom()+"',"
                + "'"+formation.getDescription()+"',"
                + "'"+formation.getCreate_date()+"',"
                + "'"+formation.getLast_modif_date()+"',"
                + "'"+formation.getDate_debut()+"',"
                + "'"+formation.getCreator_id().getId()+"',"
                + "'"+formation.getAddress_formation()+"',"
                + "'"+formation.getProfil_path()+"')";
        MyConnection.InsertNewLine(sql);
    }
    
    public static void removeFormation(int id) throws SQLException{
        /*   delete all participants */
        ParticipantDB.removeParticipantByFormation(id);
        String sql = "DELETE from formation where "
                + "formation_id = "+id; 
        MyConnection.DeleteLine(sql);
        }
    
   public static void updateFormation(Formation formation){
       String sql = "UPDATE formation SET "
                + " nom='"+formation.getNom()+"'"
                + ", description='"+formation.getDescription()+"'"
                + ", creation_date='"+formation.getCreate_date()+"'"
                + ", last_modification_date='"+formation.getLast_modif_date()+"'"
                + ", date_debut='"+formation.getDate_debut()+"'"
              
                + ", address_formation='"+formation.getAddress_formation()+"'"               
                + ", photo_profil_path='"+formation.getProfil_path()+"'"               
                +" WHERE formation_id="+formation.getFormation_id();
       MyConnection.UpdateLine(sql);
    }
   
   public static Formation getFormationById(int id) throws SQLException{
       ResultSet rs;
       String sql = "select * from formation where formation_id = "+id;
       rs = MyConnection.getObjectBySQL(sql);
       if(rs.next()){
           int id_creator = rs.getInt(7);
           Profil user = ProfilDB.getProfilById(id_creator);
           int formation_id = rs.getInt(1);
           String nom = rs.getString(2);
           String description = rs.getString(3);
           String address_formation = rs.getString(8);
           String profil_path = rs.getString(9);
           
           String create_date = rs.getString(4);
           String last_modif_date = rs.getString(5);
           String date_debut = rs.getString(6);
           Formation formation = new Formation(formation_id,nom,description,create_date,last_modif_date,date_debut,user,address_formation,profil_path);
           Profil[] participants = ParticipantDB.getParticipantsArrayByFormation(formation_id);
            for(Profil participant : participants){
                formation.addParticipant(participant);
            }
            return formation;
        } 
        return null;
    }
 /*  
   public static FormationList getFormations() throws SQLException{
       String sql = "select * from formation";
       ResultSet rs = MyConnection.getObjectBySQL(sql);
       FormationList formationList = new FormationList();
       while(rs.next()){
           int id = rs.getInt(1);
           Formation formation = getFormationById(id);
           formationList.addFormation(formation);
       }
       return formationList;
   }
   */
      public static ObservableList<Formation> getFormations() throws SQLException{
       String sql = "select * from formation";
       ResultSet rs = MyConnection.getObjectBySQL(sql);
       ObservableList<Formation> formationList = FXCollections.observableArrayList();
       while(rs.next()){
           int id = rs.getInt(1);
           Formation formation = getFormationById(id);
           formationList.add(formation);
       }
       return formationList;
   }
      
   public static ObservableList<Formation> getFormationListByCreator(int id) throws SQLException{
       String sql = "select * from formation where creator_id = "+id;
       ResultSet rs = MyConnection.getObjectBySQL(sql);
       ObservableList<Formation> formationList = FXCollections.observableArrayList();
       while(rs.next()){
               int id_formation = rs.getInt(1);
            Formation formation = getFormationById(id_formation);
            formationList.add(formation);
       }
       return formationList;
   }

}
