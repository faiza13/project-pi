/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import Technique.MyConnection;
import model.Profil;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author foufou
 */
public class ProfilDB {
    public static Profil getProfilByUser(int id)  throws SQLException{
        ResultSet rs;
        String sql = "select profil_id from profil where user_id="+id;
        rs = MyConnection.getObjectBySQL(sql);
       if(rs.next()){
           int profil_id = rs.getInt(1);
           return getProfilById(profil_id);
       }
       return null;
    }
   
   public static Profil getProfilById(int id) throws SQLException{
    ResultSet rs;
    String sql = "select * from profil where "
               + "profil_id="+id;
       rs = MyConnection.getObjectBySQL(sql);
       if(rs.next()){
           String nom = rs.getString(2);
           String prenom = rs.getString(3);
           String email = rs.getString(4);
           String tel = rs.getString(5);
           String date_naic = rs.getString(6);
           String description = rs.getString(7);           
           String guide = rs.getString(8);
           String travail = rs.getString(9);
           String creation_date = rs.getString(10);
           String last_modification = rs.getString(11);
           String photo_profil_path = rs.getString(12);
           String address = rs.getString(13);
           int user_id = rs.getInt(14);
           String sexe = rs.getString(15);           
           Profil profil = new Profil(id,nom,prenom,email,tel,date_naic,description,guide,travail,creation_date
                   ,last_modification,photo_profil_path,address,user_id,sexe);
           return profil; 
        } 
        return null;
   }

    public static Profil[] getProfils() throws SQLException {
               String sql = "select * from profil";
       ResultSet rs = MyConnection.getObjectBySQL(sql);
       List users = new LinkedList();
       while(rs.next()){
           int id = rs.getInt(1);
            Profil user = getProfilById(id);
               users.add(user);
       }
        ListIterator li = users.listIterator();
        Profil usersArray[] = new Profil[users.size()];
        int i=0;
        while(li.hasNext()){
            usersArray[i] = (Profil)li.next();
            i++;
        }
        return usersArray;
    }   
}
