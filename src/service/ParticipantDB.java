/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import Technique.MyConnection;
import model.Profil;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Formation;

/**
 *
 * @author foufou
 */
public class ParticipantDB {

    public static Profil getParticipant(int id) throws SQLException{
       return ProfilDB.getProfilById(id);
    }
    
     public static ObservableList<Profil> getParticipantsByFormation(int formation_id) throws SQLException {
       String sql = "select profil_id from formation_participant where formation_id = "+formation_id;
       ResultSet rs = MyConnection.getObjectBySQL(sql);
      
       ObservableList<Profil> participants = FXCollections.observableArrayList();
       while(rs.next()){
               int id_participant = rs.getInt(1);
               Profil participant = getParticipant(id_participant);
               participants.add(participant);
       }
        
        return participants;
    }
    
    public static Profil[] getParticipantsArrayByFormation(int formation_id) throws SQLException {
       String sql = "select profil_id from formation_participant where formation_id = "+formation_id;
       ResultSet rs = MyConnection.getObjectBySQL(sql);
       List participants = new LinkedList();
       while(rs.next()){
               int id_participant = rs.getInt(1);
               Profil participant = getParticipant(id_participant);
               participants.add(participant);
       }
        ListIterator li = participants.listIterator();
        Profil participantsArray[] = new Profil[participants.size()];
        int i=0;
        while(li.hasNext()){
            participantsArray[i] = (Profil)li.next();
            i++;
        }
        return participantsArray;
    }
    

    public static void removeParticipantByFormation(int id) throws SQLException {
    String sql = "DELETE from formation_participant where "
                + "formation_id = "+id; 
        MyConnection.DeleteLine(sql);
    }

    public static void removeParticipant(int formation_id,int id) {
    String sql = "DELETE from formation_participant where "
                + "profil_id = "+id
                + " and formation_id="+formation_id; 
        MyConnection.DeleteLine(sql);
    }

   
 public static int addParticipant(int formation_id,int participant_id) {
            String sql = "insert into formation_participant(profil_id,formation_id) values("
                + ""+participant_id+","
                + ""+formation_id+")";
        return MyConnection.InsertNewLine(sql);
    }
    
    
    public static ObservableList<Profil> getparticipantsbyformation(int formation_id) throws SQLException {
       String sql = "select profil_id from formation_participant where formation_id = "+formation_id;
       ResultSet rs = MyConnection.getObjectBySQL(sql);
       
        ObservableList<Profil> participants =FXCollections.observableArrayList();
       while(rs.next()){
               int id_participant = rs.getInt(1);
               Profil participant = getParticipant(id_participant);
               participants.add(participant);
       }
        
        return participants;
    }

public  static int etatParticipation(int formation_id) throws SQLException{
                Profil profil = ProfilDB.getProfilById(2);
                
String sql = "select * from formation_participant where formation_id = "+formation_id+" and "+"profil_id="+profil.getId();

return MyConnection.getCountRowsSQL(sql);

}



}


