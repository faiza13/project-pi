/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import Technique.MyConnection;
import model.User;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 *
 * @author foufou
 */
public class UserDB {

   public static User getUserById(int id) throws SQLException{
    ResultSet rs;
    String sql = "select * from user where user_id="+id;
       rs = MyConnection.getObjectBySQL(sql);
       if(rs.next()){
           String email = rs.getString(2);
           String user_pass = rs.getString(3);
           String creation_date = rs.getString(4);
           String last_modification_date = rs.getString(5);
           int role = rs.getInt(6);
           boolean role2 = false;
           if(role == 1) 
               role2 = true;
           User user = new User(id,email,user_pass,creation_date,last_modification_date,role2);
           return user; 
        } 
        return null;
   }
    /*
    public static void addUser(Profil user){
        String sql = "insert into user(pseudo,email,password) values("
                + "'"+user.getPseudo()+"',"
                + "'"+user.getEmail()+"',"
                + "'"+user.getPassword()+"');";
        MyConnection.InsertNewLine(sql);
    }
    
    public static void removeUser(int id){
        String sql = "DELETE from user where "
                + "id ="+id; 
        MyConnection.DeleteLine(sql);
    }
    
   public static void updateUser(Profil user){
       String sql = "UPDATE user"
                +" SET pseudo='"+user.getPseudo()+"'"
                + ", email='"+user.getEmail()+"' "
//                + ", password='"+user.getPassword()+"'" 
                +" WHERE id="+user.getId();
       MyConnection.UpdateLine(sql);
   }
   
   public static Profil getUserById(int id) throws SQLException{
    ResultSet rs;
    String sql = "select * from user where "
               + "id="+id;
       rs = MyConnection.getObjectBySQL(sql);
       if(rs.next()){
           String login = rs.getString(2);
           String email = rs.getString(3);
           String type = rs.getString(5);
           Profil user = new Profil(id,login,email,null,type);
           return user; 
        } 
        return null;
   }
   
   public static Profil Login(String login,String pass) throws SQLException{
    ResultSet rs;
    String sql = "select * from user where "
               + "pseudo='"+login+"'"
               + " and "
               + "password='"+pass+"'";
       rs = MyConnection.getObjectBySQL(sql);
       if(rs.next()){
           int id = rs.getInt(1);
           String email = rs.getString(3);
           String type = rs.getString(5);
           Profil user = new Profil(id,login,email,pass,type);
           return user; 
        } 
        return null;
   }

    public static Profil[] getUsers() throws SQLException {
               String sql = "select * from user";
       ResultSet rs = MyConnection.getObjectBySQL(sql);
       List users = new LinkedList();
       while(rs.next()){
           int id = rs.getInt(1);
            Profil user = getUserById(id);
               users.add(user);
       }
        ListIterator li = users.listIterator();
        Profil usersArray[] = new Profil[users.size()];
        int i=0;
        while(li.hasNext()){
            usersArray[i] = (Profil)li.next();
            i++;
        }
        return usersArray;
    }
*/
}
