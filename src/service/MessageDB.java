/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import Technique.MyConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Message;
import model.Formation;
import model.Profil;
import model.Discussion;
import static service.ProfilDB.getProfilById;

/**
 *
 * @author dali
 * @author foufou
 */
public class MessageDB {
   
    public static Message getMessageById(int id) throws SQLException{
        ResultSet rs;
        String sql = "select * from message where message_id="+id;
        rs = MyConnection.getObjectBySQL(sql);
       if(rs.next()){
           String contenu = rs.getString(2);
           String creation_date = rs.getString(3);
           int discussion = rs.getInt(4);
           int profil_id = rs.getInt(5);
           Profil profil = ProfilDB.getProfilById(profil_id);
           Message commentaire = new Message(id,contenu,creation_date,discussion,profil);
           return commentaire;
       }
       return null;
    }
    /*
    public static Message[] getMessagesByDiscussion(int publication_id) throws SQLException {
       String sql = "select message_id from message where discussion_id = "+publication_id;
       ResultSet rs = MyConnection.getObjectBySQL(sql);
       List messages = new LinkedList();
       while(rs.next()){
               int id_message = rs.getInt(1);
               Message message = getMessageById(id_message);
               messages.add(message);
       }
        ListIterator li = messages.listIterator();
        Message messagesArray[] = new Message[messages.size()];
        int i=0;
        while(li.hasNext()){
            messagesArray[i] = (Message)li.next();
            i++;
        }
        return messagesArray;
    }
*/
    
    public static ObservableList<Message> getMessagesByDiscussion(int publication_id) throws SQLException {
       String sql = "select message_id from message where discussion_id = "+publication_id;
       ResultSet rs = MyConnection.getObjectBySQL(sql);
       ObservableList<Message> messages = FXCollections.observableArrayList();
       while(rs.next()){
               int id_message = rs.getInt(1);
               Message message = getMessageById(id_message);
               messages.add(message);
       }
        return messages;
    }

    public static void removeMessage(int id,int publ_id) {
    String sql = "DELETE from message where "
                + "message_id = "+id
                + " and discussion_id="+publ_id; 
        MyConnection.DeleteLine(sql);
    }

    public static void addMessage(Message commentaire) {
            String sql = "insert into message(contenu,created_at,"
                    + "discussion_id,profil_id) values("
                 + "'"+commentaire.getContenu()+"',"
                + "'"+commentaire.getCreation_date()+"',"
               + "'"+commentaire.getDiscussion_id()+"',"
               + ""+commentaire.getProfil_id().getId()+")";
        MyConnection.InsertNewLine(sql);
    } 
    
       public static void updateMessage(Message commentaire){
       String sql = "UPDATE message SET "
                + " contenu='"+commentaire.getContenu()+"'"
                + ", created_at='"+commentaire.getCreation_date()+"'"
//                + ", profil_id='"+commentaire.getProfil_id().getId()+"'"             
//                + ", publication_id='"+commentaire.getPublication_id()+"'"               
                +" WHERE message_id="+commentaire.getCommentaire_id();
       MyConnection.UpdateLine(sql);
    }
}
