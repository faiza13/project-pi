/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import Technique.MyConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Discussion;
import model.Formation;
import model.Message;
import model.Profil;

/**
 *
 * @author dali
 * @author foufou
 */
public class DiscussionDB {
                public static void addDiscussion(Discussion discussion){
        String sql = "insert into discussion(profil_id,contenu,creation_date) values("
                + "'"+discussion.getProfil_id().getId()+"',"
                + "'"+discussion.getContenu()+"',"
                + "'"+discussion.getCreation_date()+"')";
        MyConnection.InsertNewLine(sql);
    }
    
    public static void removeDiscussion(int id) throws SQLException{
        /*   delete all participants */
        ParticipantDB.removeParticipantByFormation(id);
        String sql = "DELETE from discussion where "
                + "discussion_id = "+id; 
        MyConnection.DeleteLine(sql);
        }
    
   public static void updateDiscussion(Discussion discussion){
       String sql = "UPDATE discussion SET "
                + " contenu='"+discussion.getContenu()+"'"
//                + ", profil_id='"+discussion.getProfil_id().getId()+"'"               
                +" WHERE discussion_id="+discussion.getDiscussion_id();
       MyConnection.UpdateLine(sql);
    }
   
   public static Discussion getDiscussionById(int id) throws SQLException{
       ResultSet rs;
       String sql = "select * from discussion where discussion_id = "+id;
       rs = MyConnection.getObjectBySQL(sql);
       if(rs.next()){
           int id_creator = rs.getInt(2);
           Profil user = ProfilDB.getProfilById(id_creator);
           String contenu = rs.getString(3);
           String create_date = rs.getString(4);
           Discussion discussion = new Discussion(id,user,contenu,create_date);
  /*         Message[] messages = MessageDB.getMessagesByDiscussion(id);
            for(Message message : messages){
                discussion.addMessage(message);
            }
   */         return discussion;
        } 
        return null;
    }
   
      public static ObservableList<Discussion> getDiscussions() throws SQLException{
       String sql = "select * from discussion";
       ResultSet rs = MyConnection.getObjectBySQL(sql);
       ObservableList<Discussion> discussionList = FXCollections.observableArrayList();
       while(rs.next()){
           int id = rs.getInt(1);
           Discussion discussion = getDiscussionById(id);
           discussionList.add(discussion);
       }
       return discussionList;
   }
      
   public static ObservableList<Discussion> getDiscussionListByCreator(int profil_id) throws SQLException{
       String sql = "select * from discussion where profil_id = "+profil_id;
       ResultSet rs = MyConnection.getObjectBySQL(sql);
       ObservableList<Discussion> discussionList = FXCollections.observableArrayList();
       while(rs.next()){
           int id = rs.getInt(1);
           Discussion discussion = getDiscussionById(id);
           discussionList.add(discussion);
       }
       return discussionList;
   }

}
