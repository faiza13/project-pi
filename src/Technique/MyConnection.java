/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Technique;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author foufou
 */
public class MyConnection {

    /**
     *
     * @param port
     * @param sql
     * @param props
     * @return
     */
    private static final HashMap<String, String> sqlTokens;
    private static Pattern sqlTokenPattern;

    static {
        //MySQL escape sequences: http://dev.mysql.com/doc/refman/5.1/en/string-syntax.html
        String[][] search_regex_replacement = new String[][]{
            //search string     search regex        sql replacement regex
            {"\u0000", "\\x00", "\\\\0"},
            {"'", "'", "\\\\'"},
            {"\"", "\"", "\\\\\""},
            {"\b", "\\x08", "\\\\b"},
            {"\n", "\\n", "\\\\n"},
            {"\r", "\\r", "\\\\r"},
            {"\t", "\\t", "\\\\t"},
            {"\u001A", "\\x1A", "\\\\Z"},
            {"\\", "\\\\", "\\\\\\\\"}
        };
        sqlTokens = new HashMap<String, String>();
        String patternStr = "";
        for (String[] srr : search_regex_replacement) {
            sqlTokens.put(srr[0], srr[2]);
            patternStr += (patternStr.isEmpty() ? "" : "|") + srr[1];
        }
        sqlTokenPattern = Pattern.compile('(' + patternStr + ')');
    }

    public static String escape(String s) {
        Matcher matcher = sqlTokenPattern.matcher(s);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, sqlTokens.get(matcher.group(1)));
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    static final String DB_URL = "jdbc:mysql://localhost:3306/mydbnew";
    //  Paramètres d'accèss à la base des données
    static final String USER = "root";
    static final String PASS = "";

    public static Connection connect() {
        Connection conn = null;
        try {
            //Etape 2: Choisir le JDBC driver
            //Etape 3: Ouvrir une connection
            //System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            return (conn);
        } catch (SQLException se) {
            return (null);
        }
    }

    public static int getCountRowsSQL(String sql) throws SQLException {
        ResultSet rs;
        PreparedStatement stmt;
        Connection base = connect();

        
            stmt = base.prepareStatement(sql);
            rs = stmt.executeQuery();
            int count = rs.getRow();
            return count;
        

    }
    
        public static ResultSet getObjectBySQL(String sql) {
        ResultSet rs;
        PreparedStatement stmt;
        Connection base = connect();

        try {
            
            stmt = base.prepareStatement(sql);
            rs = stmt.executeQuery(sql);
            return rs;
        } catch (SQLException e) {
            Logger.getLogger(MyConnection.class.getName()).log(Level.SEVERE, "sql :{0}error :{1}", new Object[]{sql, e.getMessage()});
            return null;
        }

    }

        
        
        
        
        
    public static int InsertNewLine(String sql) {
        try {
            Connection base = connect();

            PreparedStatement stmt;
            stmt = base.prepareStatement(sql);
            stmt.executeUpdate();

            return 1;

        } catch (SQLException e) {
            e.getMessage();
            Logger.getLogger(MyConnection.class.getName()).log(Level.SEVERE, "sql :{0}error :{1}", new Object[]{sql, e.getMessage()});
            System.out.println(sql + e.getMessage());

            return (0);
        }
    }

    /**
     * *
     * @param port
     * @param sql
     * @return
     */
    public static int UpdateLine(String sql) {
        try {
            Connection base = connect();
            PreparedStatement stmt = base.prepareStatement(sql);
            stmt.executeUpdate(sql);
            return 1;
        } catch (SQLException e) {
            Logger.getLogger(MyConnection.class.getName()).log(Level.SEVERE, "sql :{0}error :{1}", new Object[]{sql, e.getMessage()});
            System.out.println(sql + e.getMessage());
            e.getMessage();
            return (0);
        }
    }

    public static int DeleteLine(String sql) {

        try {
            Connection base = connect();
            Statement stmt = base.createStatement();
            stmt.execute(sql);

            return 1;
        } catch (SQLException e) {
            e.getMessage();
            Logger.getLogger(MyConnection.class.getName()).log(Level.SEVERE, "sql :{0}error :{1}", new Object[]{sql, e.getMessage()});
            return 0;
        }

    }

    public static int InsertNewLinewithGeneratedKey(String sql) {
        try {
            Connection base = connect();
            int key = -1;
            ResultSet rs;
            Statement stmt = base.createStatement();
            stmt.execute(sql, Statement.RETURN_GENERATED_KEYS);
            rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                key = rs.getInt(1);
            }
            rs.close();
            return key;
        } catch (SQLException e) {
            e.getMessage();
            Logger.getLogger(MyConnection.class.getName()).log(Level.SEVERE, "sql :{0}error :{1}", new Object[]{sql, e.getMessage()});
            System.out.println(sql + e.getMessage());

            return 0;
        }
    }

    public static int[] InsertNewLinewithGeneratedKeys(String sql) {
        try {
            Connection base = connect();
            int key[];
            ResultSet rs;
            Statement stmt = base.createStatement();
            stmt.execute(sql, Statement.RETURN_GENERATED_KEYS);
            rs = stmt.getGeneratedKeys();
            rs.last();
            int count = rs.getRow();
            rs.first();
            key = new int[count];

            for (int i = 0; i < count; i++) {
                key[i] = rs.getInt(1);
                rs.next();

            }

            rs.close();
            return key;
        } catch (SQLException e) {
            e.getMessage();
            Logger.getLogger(MyConnection.class.getName()).log(Level.SEVERE, "sql :{0}error :{1}", new Object[]{sql, e.getMessage()});
            System.out.println(sql + e.getMessage());

            return null;
        }
    }

}
