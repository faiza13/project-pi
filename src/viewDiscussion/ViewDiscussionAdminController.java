/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewDiscussion;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import model.Discussion;

import model.Message;
import model.Profil;
import service.DiscussionDB;

import service.MessageDB;

import view.ClientPanelController;


/**
 *
 * @author foufou
 */
public class ViewDiscussionAdminController implements Initializable   {
    @FXML Label contenuVal;
    @FXML Label dateVal;
    @FXML ListView listView;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
      
     Discussion discussion ;  
        
         try {
             discussion=DiscussionDB.getDiscussionById(ClientPanelController.idPanel);
             contenuVal.setText(discussion.getContenu());
             dateVal.setText(discussion.getCreation_date());             
         } catch (SQLException ex) {
             Logger.getLogger(MesDiscussionsController.class.getName()).log(Level.SEVERE, null, ex);
         }
     ObservableList<PanelMessageController> paneList =FXCollections.observableArrayList();  
     ObservableList<Message> messageList =FXCollections.observableArrayList(); 
     
        try {
            messageList=MessageDB.getMessagesByDiscussion(ClientPanelController.idPanel);
        } catch (SQLException ex) {
            Logger.getLogger(ViewDiscussionAdminController.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        messageList.stream().forEach(e -> {
    PanelMessageController pane = new PanelMessageController();  
         
    pane.setContenuVal(e.getContenu());
    pane.setDateVal(e.getCreation_date());
    paneList.add(pane);    
     });
  
    listView.setItems(paneList);
    
   
    
            
         
     
  
    
    
     
     
     } 
   

}
