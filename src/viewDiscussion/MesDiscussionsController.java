/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewDiscussion;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Discussion;
import model.Formation;
import model.Profil;
import service.DiscussionDB;
import service.FormationDB;
import service.ProfilDB;


/**
 *
 * @author foufou
 */
public class MesDiscussionsController implements Initializable {

    static  Stage stage;
    @FXML
     
    private ListView<PanelDiscussionController> listView;
    public static int idSelected;
    public static String choix=null;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
       
     ObservableList<PanelDiscussionController> paneList =FXCollections.observableArrayList();  
     ObservableList<Discussion> discussionList =FXCollections.observableArrayList();  
        
         try {
             Profil profil=ProfilDB.getProfilById(2);
             discussionList=DiscussionDB.getDiscussionListByCreator(profil.getId());
         } catch (SQLException ex) {
             Logger.getLogger(MesDiscussionsController.class.getName()).log(Level.SEVERE, null, ex);
         }
       
        discussionList.stream().forEach(e -> {
    PanelDiscussionController pane = new PanelDiscussionController();  
    pane.setIdVal(Integer.toString(e.getDiscussion_id()));
    pane.setContenuVal(e.getContenu());
    pane.setDateVal(e.getCreation_date());
    paneList.add(pane);
   
    
            
         
     });
  
    listView.setItems(paneList);
    
    listView.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> showDiscussionDetails(newValue));  
         }
    
    
    public void refresh() {
        ObservableList<PanelDiscussionController> paneList =FXCollections.observableArrayList();  
     ObservableList<Discussion> discussionList =FXCollections.observableArrayList();  
        
         try {
             Profil profil=ProfilDB.getProfilById(2);
             discussionList=DiscussionDB.getDiscussionListByCreator(profil.getId());
         } catch (SQLException ex) {
             Logger.getLogger(MesDiscussionsController.class.getName()).log(Level.SEVERE, null, ex);
         }
       
        discussionList.stream().forEach(e -> {
    PanelDiscussionController pane = new PanelDiscussionController();  
            try {
                pane.viewAction();
                //pane.deleteAction();
            } catch (SQLException ex) {
                Logger.getLogger(MesDiscussionsController.class.getName()).log(Level.SEVERE, null, ex);
            }
    pane.setIdVal(Integer.toString(e.getDiscussion_id()));
    pane.setContenuVal(e.getContenu());
    pane.setDateVal(e.getCreation_date());
    paneList.add(pane);    
     });
  
    listView.setItems(paneList);
    
    listView.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> showDiscussionDetails(newValue));
    }
    
  
 private void showDiscussionDetails(PanelDiscussionController discussion) {
    if (discussion != null) {
   this.idSelected= Integer.parseInt(discussion.getIdVal());
    } else {
        
    }
}
 
     @FXML
    public void editAction() throws IOException{
        int selectedIndex = listView.getSelectionModel().getSelectedIndex();
    if (selectedIndex >= 0){
     choix="edit";
    Parent root; 
   stage = new Stage();
   root = FXMLLoader.load(getClass().getResource("EditAjoutPopup.fxml"));

   stage.setScene(new Scene(root));
   stage.setTitle("Edit discussion");
   stage.initModality(Modality.APPLICATION_MODAL);
 
   stage.showAndWait(); }
    else {
        // Nothing selected.
        Alert alert = new Alert(Alert.AlertType.WARNING);
        
        alert.setTitle("pas de Selection");
        alert.setHeaderText("aucune discusion Selectionnée");
        alert.setContentText("veuillez selectionner une discusion");

        alert.showAndWait();
    }

       
   }

    
    @FXML
    public void ajoutAction() throws IOException{
        choix="ajout";
        Parent root; 
   stage = new Stage();
   root = FXMLLoader.load(getClass().getResource("EditAjoutPopup.fxml"));
   stage.setScene(new Scene(root));
   stage.setTitle("Ajot discussion");
   stage.initModality(Modality.APPLICATION_MODAL);
   stage.showAndWait();
   }
 
 
   
    
}
