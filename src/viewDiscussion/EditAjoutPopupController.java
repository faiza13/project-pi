/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewDiscussion;

import Commun.Commun;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import model.Discussion;
import model.Profil;
import service.DiscussionDB;
import service.ProfilDB;


/**
 *
 * @author foufou
 */
public class EditAjoutPopupController implements Initializable{
        @FXML
    private TextField id;
    @FXML
    private TextField contenu;
    @FXML
    private Button btn;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        id.setVisible(false);
        if(MesDiscussionsController.choix=="edit"){
        try {
            FillFields(MesDiscussionsController.idSelected);
        } catch (ParseException ex) {
            Logger.getLogger(view.EditAjoutPopupController.class.getName()).log(Level.SEVERE, null, ex);
        }
        btn.setText("Edit");
           }
        else if (MesDiscussionsController.choix=="ajout"){
            id.setText(null);
           btn.setText("Add") ;  }       }

    public void FillFields(int id_discussion) throws ParseException{
        Discussion discussion;
        try {
            discussion = DiscussionDB.getDiscussionById(id_discussion);
            id.setText(Integer.toString(discussion.getDiscussion_id()));
            contenu.setText(discussion.getContenu());
        } catch (SQLException ex) {
            Logger.getLogger(view.EditAjoutPopupController.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }

    @FXML
    public void buttonAction() throws SQLException, IOException{   
                Profil profil = ProfilDB.getProfilById(2);
                
                if(id.getText()==null){
                Discussion discussion = new Discussion(0,profil,contenu.getText(),Commun.currentDate());
                DiscussionDB.addDiscussion(discussion);
                MesDiscussionsController.choix=null;
                }
                else{
                Discussion discussion = new Discussion(Integer.parseInt(id.getText()),profil,contenu.getText(),Commun.currentDate());
                DiscussionDB.updateDiscussion(discussion);
                MesDiscussionsController.choix=null;
     }
FXMLLoader loader=new FXMLLoader();
    loader.setLocation(MesDiscussionsController.class.getResource("MesDiscusions_1.fxml"));
    
    Scene scene =new Scene(loader.load(),800,600);
    testMesDiscussions.stage.setScene(scene);
    MesDiscussionsController.stage.close();
       
   }    

}


