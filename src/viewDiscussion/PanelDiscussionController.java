/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewDiscussion;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import static view.ClientPanelController.idPanel;
import static viewDiscussion.MesDiscussionsController.stage;


/**
 *
 * @author foufou
 */
public class PanelDiscussionController extends HBox {
    @FXML private Label idVal;
    @FXML private Label contenuVal;
    @FXML private Label dateVal;
    @FXML private Button view;
    @FXML private Button delete;
    public static int idPanel;
    
    public PanelDiscussionController() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
        "PanelDiscussion.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public String getIdVal() {
        return idVal.getText();
    }
    public String getContenuVal() {
        return contenuVal.getText();
    }

    public String getDateVal() {
        return dateVal.getText();
    }


    public void setContenuVal(String value) {
        contenuVal.setText(value);
    }

    public void setDateVal(String value) {
        dateVal.setText(value);
    }

    public void setIdVal(String value) {
        idVal.setText(value);
    }
protected void viewAction() throws SQLException{

        view.setOnAction((event) -> {
    
 
    Parent root;
 
   stage = new Stage();
            try {
   idPanel=Integer.parseInt(idVal.getText());

   root = FXMLLoader.load(getClass().getResource("ViewFormationAdmin.fxml"));
                
                
   stage.setScene(new Scene(root));
   stage.setTitle("Affiche formation");
   stage.initModality(Modality.APPLICATION_MODAL);
 
   stage.showAndWait(); 
            
       
            
            } catch (IOException ex) {
             //   Logger.getLogger(ClientPanelController.class.getName()).log(Level.SEVERE, null, ex);
            }

   
});       
}    

}
