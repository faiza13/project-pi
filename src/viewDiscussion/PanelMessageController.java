/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewDiscussion;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 *
 * @author foufou
 */
public class PanelMessageController extends HBox {
    @FXML Label contenuVal;
    @FXML Label dateVal;  
    public PanelMessageController() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
        "PanelMessage.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }
    
     public String getContenuVal() {
        return contenuVal.getText();
    }
    public String getDateVal() {
        return dateVal.getText();
    }

     public void setContenuVal(String value) {
        contenuVal.setText(value);
    }

    public void setDateVal(String value) {
        dateVal.setText(value);
    }


}
