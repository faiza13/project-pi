/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author foufou
 */
public class User {
    private int user_id;
    private String email;
    private String user_pass;
    private String creation_date;
    private String last_modification_date;
    private boolean role;

    public User(int user_id, String email, String user_pass, String creation_date, String last_modification_date, boolean role) {
        this.user_id = user_id;
        this.email = email;
        this.user_pass = user_pass;
        this.creation_date = creation_date;
        this.last_modification_date = last_modification_date;
        this.role = role;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUser_pass() {
        return user_pass;
    }

    public void setUser_pass(String user_pass) {
        this.user_pass = user_pass;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getLast_modification_date() {
        return last_modification_date;
    }

    public void setLast_modification_date(String last_modification_date) {
        this.last_modification_date = last_modification_date;
    }

    public boolean isRole() {
        return role;
    }

    public void setRole(boolean role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" + "user_id=" + user_id + ", email=" + email + ", user_pass=" + user_pass + ", creation_date=" + creation_date + ", last_modification_date=" + last_modification_date + ", role=" + role + '}';
    }
    
}
