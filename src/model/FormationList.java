/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

/**
 *
 * @author foufou
 */
public class FormationList {
    private Map formation;

    public FormationList() {
        this.formation = new HashMap();
    }

    public void addFormation(Formation d){
        formation.put(d.getFormation_id(), d);
    }
/*    
    public void removeFormation(int id){
        formation.remove(id);
    }
*/   
    public Set getFormationList() {
        return formation.entrySet();
    }

    public Formation[] getFormationArray(){
        Formation[] formationArray = new Formation[formation.size()];
        Set formationList = getFormationList();
        int i=0;
        Iterator formationIt = formationList.iterator();
        while(formationIt.hasNext()){
            Map.Entry me = (Map.Entry) formationIt.next();
            formationArray[i] = (Formation)me.getValue();
            i++;
        }
        return formationArray;
    }
    
    public Formation[] getFormationByParticipant(int id){
        Formation [] formations = getFormationArray();
        Vector<Formation> d = new Vector<Formation>();
        for(int i=0;i<formations.length;i++){
            if(formations[i].participantExists(id)){
                d.add(formations[i]);
            }
        }
        return (Formation[]) d.toArray();
    }
}
