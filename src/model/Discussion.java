/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author dali
 * @author foufou
 */
public class Discussion {
    private int discussion_id;
    private Profil profil_id;
    private String contenu;
    private String creation_date;
    private Map messages = new HashMap();

    public Discussion(int discussion_id, Profil profil_id, String contenu, String creation_date) {
        this.discussion_id = discussion_id;
        this.profil_id = profil_id;
        this.contenu = contenu;
        this.creation_date = creation_date;
    }

    public int getDiscussion_id() {
        return discussion_id;
    }

    public void setDiscussion_id(int discussion_id) {
        this.discussion_id = discussion_id;
    }

    public Profil getProfil_id() {
        return profil_id;
    }

    public void setProfil_id(Profil profil_id) {
        this.profil_id = profil_id;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }
    
        public void addMessage(Message message){
        messages.put(message.getCommentaire_id(), message);
    }
    
    public Object removeChatter(int id){
        return messages.remove(id);
    }
    
    
    public Message getMessage(int id){
        return (Message)messages.get(id);
    }
    
    public boolean messageExists(int id){
        return messages.containsKey(id);
    }
    
        public int getNumMessages(){
        return messages.size();
    }
    
    public Set getMessages(){
        return messages.entrySet();
    }
    
    public Message[] getMessagesArray(){
        Message[] messagesArray = new Message[getNumMessages()];
        Set messages = getMessages();
        Iterator messagesIt = messages.iterator();
        int i = 0;
        while(messagesIt.hasNext()){
            Map.Entry me = (Map.Entry) messagesIt.next();
            Integer Key = (Integer) me.getKey();
            messagesArray[i] = (Message)me.getValue();
            i++;
        }
        return messagesArray;
    }

    @Override
    public String toString() {
        return "Discussion{" + "discussion_id=" + discussion_id + ", profil_id=" + profil_id + ", contenu=" + contenu + ", creation_date=" + creation_date + '}';
    }

}
