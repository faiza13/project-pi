/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author foufou
 */
public class Formation {
    private int formation_id;
    private String nom;
    private String description;
    private String create_date;
    private String last_modif_date;
    private String date_debut;
    private Profil creator_id;
    private String address_formation;
    private String profil_path;
    private Map participants = new HashMap();
    
    public Formation(int formation_id, String nom, String description, String create_date, String last_modif_date, String date_debut, Profil creator_id, String address_formation, String profil_path) {
        this.formation_id = formation_id;
        this.nom = nom;
        this.description = description;
        this.create_date = create_date;
        this.last_modif_date = last_modif_date;
        this.date_debut = date_debut;
        this.creator_id = creator_id;
        this.address_formation = address_formation;
        this.profil_path = profil_path;
    }

    public int getFormation_id() {
        return formation_id;
    }

    public void setFormation_id(int formation_id) {
        this.formation_id = formation_id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getLast_modif_date() {
        return last_modif_date;
    }

    public void setLast_modif_date(String last_modif_date) {
        this.last_modif_date = last_modif_date;
    }

    public String getDate_debut() {
        return date_debut;
    }

    public void setDate_debut(String date_debut) {
        this.date_debut = date_debut;
    }

    public Profil getCreator_id() {
        return creator_id;
    }

    public void setCreator_id(Profil creator_id) {
        this.creator_id = creator_id;
    }

    public String getAddress_formation() {
        return address_formation;
    }

    public void setAddress_formation(String address_formation) {
        this.address_formation = address_formation;
    }

    public String getProfil_path() {
        return profil_path;
    }

    public void setProfil_path(String profil_path) {
        this.profil_path = profil_path;
    }
    
        public void addParticipant(Profil participant){
        participants.put(participant.getId(), participant);
    }
    
    public Object removeChatter(int id){
        return participants.remove(id);
    }
    
    
    public Profil getParticipant(int id){
        return (Profil)participants.get(id);
    }
    
    public boolean participantExists(int id){
        return participants.containsKey(id);
    }
    
        public int getNumParticipants(){
        return participants.size();
    }
    
    public Set getParticipants(){
        return participants.entrySet();
    }
    
    public Profil[] getParticipantsArray(){
        Profil[] participantsArray = new Profil[getNumParticipants()];
        Set participants = getParticipants();
        Iterator participantsIt = participants.iterator();
        int i = 0;
        while(participantsIt.hasNext()){
            Map.Entry me = (Map.Entry) participantsIt.next();
            Integer Key = (Integer) me.getKey();
            participantsArray[i] = (Profil)me.getValue();
            i++;
        }
        return participantsArray;
    }

    @Override
    public String toString() {
        return "Formation{" + "formation_id=" + formation_id + ", nom=" + nom + ", description=" + description + ", create_date=" + create_date + ", last_modif_date=" + last_modif_date + ", date_debut=" + date_debut + ", creator_id=" + creator_id + ", address_formation=" + address_formation + ", profil_path=" + profil_path + ", participants=" + participants + '}';
    }
    
}
