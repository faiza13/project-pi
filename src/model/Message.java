/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author dali
 * @author foufou
 */
public class Message {
    private int commentaire_id;
    private String contenu;
    private String creation_date;
    private int discussion_id;
    private Profil profil_id;

    public Message(int commentaire_id, String contenu, String creation_date, int discussion_id, Profil profil_id) {
        this.commentaire_id = commentaire_id;
        this.contenu = contenu;
        this.creation_date = creation_date;
        this.discussion_id = discussion_id;
        this.profil_id = profil_id;
    }

    public int getCommentaire_id() {
        return commentaire_id;
    }

    public void setCommentaire_id(int commentaire_id) {
        this.commentaire_id = commentaire_id;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public int getDiscussion_id() {
        return discussion_id;
    }

    public void setDiscussion_id(int discussion_id) {
        this.discussion_id = discussion_id;
    }

    public Profil getProfil_id() {
        return profil_id;
    }

    public void setProfil_id(Profil profil_id) {
        this.profil_id = profil_id;
    }

    @Override
    public String toString() {
        return "Message{" + "commentaire_id=" + commentaire_id + ", contenu=" + contenu + ", creation_date=" + creation_date + ", discussion_id=" + discussion_id + ", profil_id=" + profil_id + '}';
    }
    

}
