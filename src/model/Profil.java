/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author foufou
 */
public class Profil {
    private int id;
    private String nom;
    private String prenom;
    private String email;
    private String tel;
    private String date_naic;
    private String description;
    private String guide;
    private String travail;
    private String creation_date;
    private String last_modification_date;
    private String photo_profil_path;
    private String address;
    private int user_id;
    private String sexe;

    public Profil(int id, String nom, String prenom, String email, String tel, String date_naic, String description, String guide, String travail, String creation_date, String last_modification_date, String photo_profil_path, String address, int user_id, String sexe) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.tel = tel;
        this.date_naic = date_naic;
        this.description = description;
        this.guide = guide;
        this.travail = travail;
        this.creation_date = creation_date;
        this.last_modification_date = last_modification_date;
        this.photo_profil_path = photo_profil_path;
        this.address = address;
        this.user_id = user_id;
        this.sexe = sexe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getDate_naic() {
        return date_naic;
    }

    public void setDate_naic(String date_naic) {
        this.date_naic = date_naic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGuide() {
        return guide;
    }

    public void setGuide(String guide) {
        this.guide = guide;
    }

    public String getTravail() {
        return travail;
    }

    public void setTravail(String travail) {
        this.travail = travail;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getLast_modification_date() {
        return last_modification_date;
    }

    public void setLast_modification_date(String last_modification_date) {
        this.last_modification_date = last_modification_date;
    }

    public String getPhoto_profil_path() {
        return photo_profil_path;
    }

    public void setPhoto_profil_path(String photo_profil_path) {
        this.photo_profil_path = photo_profil_path;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    @Override
    public String toString() {
        return "Profil{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", email=" + email + ", tel=" + tel + ", date_naic=" + date_naic + ", description=" + description + ", guide=" + guide + ", travail=" + travail + ", creation_date=" + creation_date + ", last_modification_date=" + last_modification_date + ", photo_profil_path=" + photo_profil_path + ", address=" + address + ", user_id=" + user_id + ", sexe=" + sexe + '}';
    }

}
