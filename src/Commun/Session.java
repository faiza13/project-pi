/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commun;

import service.UserDB;
import model.User;
import java.sql.SQLException;

/**
 *
 * @author foufou
 */
public class Session {
    User user;
    public Session(int id) throws SQLException{
        user = UserDB.getUserById(id);
    }
    public User getSession(){
        return user;
    }
}
