/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commun;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import javafx.scene.control.DatePicker;
/**
 *
 * @author foufou
 */
public class Commun {
    public static String currentDate(){
       DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
       Date date = new Date();
    return dateFormat.format(date); 
    }
    
    public static String datePickeToString(DatePicker datepicker){
        return datepicker.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }
}
